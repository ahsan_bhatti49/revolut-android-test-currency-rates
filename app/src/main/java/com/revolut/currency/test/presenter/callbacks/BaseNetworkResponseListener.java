package com.revolut.currency.test.presenter.callbacks;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by ahsan on 16/10/2018.
 */

public interface BaseNetworkResponseListener {
    void getApiResponse(Context context);
}
