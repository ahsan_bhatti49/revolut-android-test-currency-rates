package com.revolut.currency.test.presenter.implementators;

import android.content.Context;

import com.revolut.currency.test.contractor.BaseNetworkContractor;
import com.revolut.currency.test.model.NetworkCommunicator.NetworkCommunicationInteractor;
import com.revolut.currency.test.model.NetworkCommunicator.NetworkResponseListener;
import com.revolut.currency.test.model.dao.NetworkCommunicationModel;
import com.revolut.currency.test.presenter.callbacks.BaseNetworkResponseListener;

import org.json.JSONObject;

/**
 * Created by ahsan on 16/10/2018.
 */

public class BaseImplementator implements BaseNetworkResponseListener, NetworkResponseListener.onResponseListener {
    private BaseNetworkContractor baseNetworkContractor;
    private NetworkResponseListener networkResponseListener;

    public BaseImplementator(BaseNetworkContractor ckUserContractor) {
        this.baseNetworkContractor = ckUserContractor;
        networkResponseListener = new NetworkCommunicationInteractor();
    }

    @Override
    public void onError() {
        baseNetworkContractor.onError("Server is not responding");
    }

    @Override
    public void onSuccess(NetworkCommunicationModel response) {
        baseNetworkContractor.getApiResult(response);
    }

    @Override
    public void onFailure(String message) {
        baseNetworkContractor.onError(message);
    }

    @Override
    public void getApiResponse(Context context) {
        networkResponseListener.getResponse(context, this);

    }
}
