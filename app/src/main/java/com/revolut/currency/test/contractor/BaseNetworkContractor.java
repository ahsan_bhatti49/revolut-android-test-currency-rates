package com.revolut.currency.test.contractor;

import com.revolut.currency.test.model.dao.NetworkCommunicationModel;

/**
 * Created by ahsan on 16/10/2018.
 */

public interface BaseNetworkContractor {

    void onError(String errorMsg);

    void getApiResult(NetworkCommunicationModel response);

}

