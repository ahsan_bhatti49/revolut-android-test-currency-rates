package com.revolut.currency.test.model.NetworkCommunicator;

import android.content.Context;

import com.revolut.currency.test.model.dao.NetworkCommunicationModel;

import org.json.JSONObject;

/**
 * Created by ahsan on 11/09/2018.
 */

public interface NetworkResponseListener {
    interface onResponseListener {
        void onError();

        void onSuccess(NetworkCommunicationModel response);

        void onFailure(String message);

    }

    void getResponse(Context context, onResponseListener onResponseListener);
}
