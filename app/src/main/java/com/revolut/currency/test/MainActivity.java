package com.revolut.currency.test;

import android.content.Context;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.revolut.currency.test.adapter.CurrencyRatesAdapter;
import com.revolut.currency.test.contractor.BaseNetworkContractor;
import com.revolut.currency.test.model.dao.NetworkCommunicationModel;
import com.revolut.currency.test.presenter.implementators.BaseImplementator;

import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity implements BaseNetworkContractor, CurrencyRatesAdapter.onItemClickListener {

    private RecyclerView rvCurrencyRates;
    private BaseImplementator baseImplementator = null;
    private CurrencyRatesAdapter ratesAdapter;
    private LinkedHashMap<String, String> currencyRates;
    private String mKey = null;
    private final Handler mHandler = new Handler();
    private boolean startHandler = true;

    private Runnable runnable = new Runnable() {

        @Override
        public void run() {
            if (startHandler) {
                getCurrencyRates();
            }

            mHandler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initMVP();
        setKeyboardVisibilityListener();
        setupHandler();

    }

    private void setupHandler() {

        mHandler.postDelayed(runnable, 1000);
    }

    private void initUI() {
        rvCurrencyRates = findViewById(R.id.rvCurrencyRates);
        setUpDecoration();


    }

    private void setUpDecoration() {
        SnappingLinearLayoutManager mLayoutManager = new SnappingLinearLayoutManager(this, DividerItemDecoration.VERTICAL, false);
        rvCurrencyRates.setLayoutManager(mLayoutManager);
        rvCurrencyRates.addItemDecoration(new ItemDecorator(this, DividerItemDecoration.VERTICAL, 16));
    }

    /**
     *
     */
    private void initMVP() {
        baseImplementator = new BaseImplementator(this);
    }


    private void getCurrencyRates() {
        if (isNetworkAvailable()) {
            baseImplementator.getApiResponse(this);
        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * @return
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    @Override
    public void onError(String errorMsg) {
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void getApiResult(NetworkCommunicationModel response) {
        currencyRates = response.getReates();
        if (mKey != null) {
            setupTopDataSet();
//            ratesAdapter.setTextWatcher((CurrencyRatesAdapter.CurrencyRateViewHolder) rvCurrencyRates.findViewHolderForAdapterPosition(0));
        }
        ratesAdapter = new CurrencyRatesAdapter(this, currencyRates);
        rvCurrencyRates.setAdapter(ratesAdapter);


    }

    /**
     *
     */
    private void setupTopDataSet() {
        LinkedHashMap<String, String> newMap = new LinkedHashMap<>(currencyRates.size());
        String value = currencyRates.get(mKey);

        currencyRates.remove(mKey);
        newMap.put(mKey, value);
        newMap.putAll(currencyRates);

        currencyRates.clear();
        currencyRates.putAll(newMap);
    }

    @Override
    public void onItemClick(String key) {

        mKey = key;
        setupTopDataSet();
        ratesAdapter.updateDataSet(currencyRates);
    }

    @Override
    public void stopHandler() {
        startHandler = false;
        mHandler.removeCallbacks(runnable);
    }

    @Override
    public void startHandler() {
        startHandler = true;
        setupHandler();
    }


    public void setKeyboardVisibilityListener() {

        try {
            final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
            parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                private boolean alreadyOpen;
                private final int defaultKeyboardHeightDP = 100;
                private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + 48;
                private final Rect rect = new Rect();

                @Override
                public void onGlobalLayout() {
                    int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
                    parentView.getWindowVisibleDisplayFrame(rect);
                    int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
                    boolean isShown = heightDiff >= estimatedKeyboardHeight;

                    if (isShown == alreadyOpen) {
                        return;
                    }
                    if (isShown) {
                        stopHandler();
//                        ratesAdapter.setTextWatcher((CurrencyRatesAdapter.CurrencyRateViewHolder) rvCurrencyRates.findViewHolderForAdapterPosition(0));
                    } else {
//                        ratesAdapter.diableTextWatcher((CurrencyRatesAdapter.CurrencyRateViewHolder) rvCurrencyRates.findViewHolderForAdapterPosition(0));
                        startHandler();
                    }
                    alreadyOpen = isShown;
                }
            });
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }
}
