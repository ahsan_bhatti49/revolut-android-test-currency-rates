package com.revolut.currency.test.model.retrofit;


import com.revolut.currency.test.model.dao.NetworkCommunicationModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by ahsan on 11/09/2018.
 */

public interface NetworkCalls {

    @GET("/latest?base=EUR")
    Observable<NetworkCommunicationModel> getResponse();
}
