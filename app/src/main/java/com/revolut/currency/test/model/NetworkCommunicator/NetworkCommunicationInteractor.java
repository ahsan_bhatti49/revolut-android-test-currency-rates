package com.revolut.currency.test.model.NetworkCommunicator;

import android.content.Context;
import android.support.annotation.NonNull;

import com.revolut.currency.test.R;
import com.revolut.currency.test.model.dao.NetworkCommunicationModel;
import com.revolut.currency.test.model.retrofit.NetworkCalls;
import com.revolut.currency.test.model.retrofit.RetrofitClient;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ahsan on 16/10/2018.
 */

public class NetworkCommunicationInteractor implements NetworkResponseListener {

    private NetworkCommunicationModel finalResponse;
    private onResponseListener responseListener;


    /**
     * @return
     */
    private io.reactivex.Observable<NetworkCommunicationModel> getObservable() {

        return RetrofitClient.getRetrofitClient()
                .create(NetworkCalls.class)
                .getResponse()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()
                );
    }


    /**
     * @return
     */
    private DisposableObserver<NetworkCommunicationModel> getObserver() {

        return new DisposableObserver<NetworkCommunicationModel>() {
            @Override
            public void onNext(@NonNull NetworkCommunicationModel response) {
                finalResponse = response;
            }

            @Override
            public void onError(@NonNull Throwable e) {
                responseListener.onFailure("Network connection failed. Please verify your connection and try again.");
            }

            @Override
            public void onComplete() {
                try {
                    if ((!finalResponse.equals("")) && responseListener != null) {
                        responseListener.onSuccess(finalResponse);
                    }
                } catch (Exception e) {
                    responseListener.onFailure("Something went wrong please try again");
                }
            }
        };
    }

    /**
     * @param context
     * @param onResponseListener
     */

    @Override
    public void getResponse(Context context, onResponseListener onResponseListener) {
        if (onResponseListener != null) {
            responseListener = onResponseListener;
            getObservable().subscribeWith(getObserver());

        }
    }
}

