package com.revolut.currency.test.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.revolut.currency.test.R;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class CurrencyRatesAdapter extends RecyclerView.Adapter<CurrencyRatesAdapter.CurrencyRateViewHolder> {


    private LinkedHashMap<String, String> currencyRates;
    private onItemClickListener listener;

    private double baseCurrency = 1;
    private double prevBaseCurr = 0;

    DecimalFormat df = new DecimalFormat("#.##");

    public CurrencyRatesAdapter(onItemClickListener onItemClickListener, LinkedHashMap<String, String> rates) {
        this.currencyRates = rates;
        this.listener = onItemClickListener;

    }

    @NonNull
    @Override
    public CurrencyRateViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rv_currency_rates_item, viewGroup, false);

        return new CurrencyRateViewHolder(itemView, new MyCustomEditTextListener());
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onBindViewHolder(@NonNull CurrencyRateViewHolder holder, int i) {
        try {

            holder.setPosition(i);

        } catch (Exception e) {
            Log.d(CurrencyRatesAdapter.class.getName(), e.getLocalizedMessage());
        }
    }

    /**
     *
     */
    private void updateDataValues(double value) {
        int i = 0;
        if (value == 0) {
            return;
        }
        for (Map.Entry<String, String> entry : currencyRates.entrySet()) {

            if (i != 0) {
                currencyRates.put(entry.getKey(), String.valueOf((Double.parseDouble(entry.getValue()) * value)));
                notifyItemChanged(i);
            }
            i++;
        }
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return currencyRates.size();
    }

    public void updateDataSet(LinkedHashMap<String, String> updatedData) {
        this.currencyRates = updatedData;
        this.baseCurrency = 1;
        notifyItemRangeRemoved(0, currencyRates.size());
        notifyDataSetChanged();
    }

    public class CurrencyRateViewHolder extends ViewHolder {
        TextView tvTitle, tvRate;
        EditText etRate;
        MyCustomEditTextListener myCustomEditTextListener;

        CurrencyRateViewHolder(View view, MyCustomEditTextListener myCustomEditTextListener) {
            super(view);

            tvTitle = view.findViewById(R.id.tvCurrencyTitle);
            etRate = view.findViewById(R.id.etRate);
            tvRate = view.findViewById(R.id.tvrate);
            this.myCustomEditTextListener = myCustomEditTextListener;

            view.setOnClickListener(v -> listener.onItemClick(Objects.requireNonNull(currencyRates.keySet().toArray())[getAdapterPosition()].toString()));

        }

        public void setPosition(int i) {
            String key = Objects.requireNonNull(currencyRates.keySet().toArray())[i].toString();
            tvTitle.setText(key);
            etRate.setText(String.valueOf(Double.parseDouble(currencyRates.get(key))));
            if (i == 0) {
                etRate.addTextChangedListener(myCustomEditTextListener);
            }

        }


    }

    class MyCustomEditTextListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
            if (!editable.toString().isEmpty()) {

                if (!editable.toString().equals("0") ||
                        editable.toString().equals("0.0") ||
                        editable.toString().equals("0.00")) {
                    if (baseCurrency != 0.0) {
                        prevBaseCurr = baseCurrency;
                        baseCurrency = Double.valueOf(df.format(baseCurrency / Double.parseDouble(editable.toString())));
                        updateDataValues(baseCurrency);
                    } else {
                        updateDataValues(prevBaseCurr);
                    }

                } else {
                    updateDataValues(0);
                }


            }

        }
    }

    public interface onItemClickListener {
        void onItemClick(String key);

        void stopHandler();

        void startHandler();
    }

}
